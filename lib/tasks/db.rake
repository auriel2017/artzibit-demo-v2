namespace :db do
  desc "Update Artwork's total fields"
  task update_artwork_fields: [:environment, :create] do
    artworks = Artwork.all
    artworks.each do |a|
      artwork = Artwork.find_by id: a.id
      unless artwork.blank?
        artwork.total_sales = artwork.sales.round(2)
        artwork.total_pieces_sold = artwork.pieces_sold
        artwork.total_favorites = artwork.total_favorites
        if artwork.save(validate: false)
          puts "Artwork:#{a.id}: Saved"
        else
          puts "Artwork:#{a.id}: #{artwork.errors.full_messages.first}"
        end
      end
    end
  end
end