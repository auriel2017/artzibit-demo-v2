ActiveAdmin.register User do
  menu if: proc{ current_admin_user.admin? }
  actions :all
  filter :email
  filter :name
  filter :country
  index do
    selectable_column
    column "Rank when registered" do |user|
      user.artwork_sequence_number
    end
    column :id
    column "Image" do |user|
      image_tag url_for(user.avatar.url), size: "50x50"
    end
    column :email
    column :name
    column :username
    column :country
    column :artist_type
    column "Facebook" do |user|
      user.user_authentication.present?
    end
    column "Notify" do |user|
      user.notify
    end
    column "Verified" do |user|
      user.verified
    end
    column "" do |row|
      if row.verified
        (link_to "Unverify", verify_admin_user_path(row), method: :put)
      else
        if row.user_verified.blank?
          (link_to "Verify", verify_admin_user_path(row), method: :post)
        else
          (link_to "Verify", verify_admin_user_path(row), method: :put)
        end
      end
    end
    column "Action" do |row|
      (link_to "Add Shipping Details", new_admin_user_shipping_detail_path(user_id: row), method: :get)
    end
    actions
  end
  action_item only: :show do
    if resource.verified
      (link_to "Unverify", verify_admin_user_path(resource), method: :put)
    else
      if resource.user_verified.blank?
        (link_to "Verify", verify_admin_user_path(resource), method: :post)
      else
        (link_to "Verify", verify_admin_user_path(resource), method: :put)
      end
    end
  end
  action_item only: :show do
    (link_to "Add Shipping Details", new_admin_user_shipping_detail_path(user_id: resource), method: :get)
  end
  member_action :verify, method: [:post, :put] do
    user = User.find(params[:id])
    if request.post?
      user_verify = user.verify
    else
      user_verify = user.unverify
    end
    if user_verify.save
      redirect_to({action: :index}, notice: "You successfully updated the user.")
    else
      redirect_to({action: :index}, alert: user_unverify.errors.full_messages.first )
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row :email
        row :name
        row :username
        row :country
        row "Artist Type" do |user|
          "#{User::ARTIST_TYPE[user.artist_type]}"
        end
        row "Facebook" do |user|
          resource.user_authentication.present?
        end
        row "Notify" do |user|
          resource.notify
        end
        row "Image" do
          image_tag url_for(resource.avatar.url)
        end
        row "Stripe ID" do
          resource.stripe_details
        end
      end
    end
    panel "Shipping Details" do
      table_for resource.user_shipping_details.order(:updated_at) do
        column :is_default
        column :id
        column :country
        column "Name" do |u|
          "#{u.first_name} #{u.last_name}"
        end
        column :phone_number
        column "Detailed Address" do |u|
          link_to "#{u.detailed_addr}", admin_user_shipping_detail_path(u)
        end
      end
    end
  end
  permit_params :id, :email, :password, :password_confirmation,
    :remember_me, :name, :username, :country, :avatar, :notify, :artist_type,
    :by_pass_activation_code
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :name
      f.input :username
      f.input :country, as: :string
      f.input :by_pass_activation_code, as: :boolean
      f.input :notify, as: :boolean
      f.input :artist_type, as: :radio, collection: [["Not an Artist",""],["Photographer","photographer"],
        ["Visual Artist", "visual_artist"]]
    end
    f.actions
  end
  controller do
    def update_resource(object, attributes)
      update_method = attributes.first[:password].present? ? :update_attributes : :update_without_password
      object.send(update_method, *attributes)
    end
  end
end