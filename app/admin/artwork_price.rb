# ActiveAdmin.register ArtworkPrice do
#   menu parent: 'Artworks', label: "Price"
#   form do |f|
#     @artwork = controller.instance_variable_get(:@artwork)
#     @artwork = Artwork.all if @artwork.blank?
#     @artwork_prices = controller.instance_variable_get(:@artwork_prices)
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :artwork, input_html: { class: "chosen-input", style: "width:75% !important"},
#         collection: @artwork, prompt: true, include_blank: true, allow_blank: true,
#         selected: f.object.artwork_id
#       f.input :artwork_size, as: :select, prompt: true, include_blank: true, allow_blank: true, collection:
#         option_groups_from_collection_for_select(Artwork.all, :artwork_sizes, :name, :id, :format_size, f.object.artwork_size_id)
#       f.input :price, label: "#{'Base' if @artwork.first.scalable?} Price #{f.object.artwork_size_id} "
#       f.input :effectivity_date, as: :date_picker, input_html:
#         {style: "width:75% !important"}
#     end
#     f.actions
#     panel "Price Historyx" do
#       unless @artwork_prices.blank?
#         table_for @artwork_prices do
#           column :price
#           column :effectivity_date
#         end
#       else
#         "No added price yet"
#       end
#     end
#   end
#   permit_params :id, :price, :effectivity_date, :artwork_id, :artwork_size_id
#   controller do
#     def new
#       @artwork = Artwork.where id: params[:artwork_id]
#       unless @artwork.blank?
#         @artwork_prices = @artwork.first.artwork_prices.order("effectivity_date desc")
#       end
#       super
#     end
#     def create
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#     def update
#       super do |success,failure|
#         success.html { redirect_to admin_artwork_path(resource.artwork) }
#       end
#     end
#   end
# end