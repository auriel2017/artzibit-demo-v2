# ActiveAdmin.register Event do
#   actions :all
#   index do
#     selectable_column
#     column :id
#     column :title
#     column :location
#     actions
#   end
#   show do
#     panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
#       attributes_table_for resource do
#         row :id
#         row :title
#         row :location
#         row "Coordinates" do
#           "#{resource.long} - #{resource.lat}"
#         end
#         row "Artworks" do
#           resource.artworks.map{|b| [b.name, (b.artwork_images.blank? ? "(No Image)" : (image_tag url_for(b.featured_image.try(:url)),
#             size: "150x150"))] }.join("&nbsp;<br/>").
#             html_safe unless resource.artworks.blank?
#         end
#       end
#     end
#     panel "Date" do
#       table_for resource.event_batches.order("start_date desc") do
#         column :start_date
#         column :end_date
#       end
#     end
#   end
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :title
#       f.input :description, as: :text
#       f.input :location, as: :string
#       f.input :long
#       f.input :lat
#       f.input :artwork_ids, collection: Artwork.all.map{|u|[u.name, u.id]},
#         input_html: { style: "width:450px !important;", class: "chosen-select" },
#         multiple: true, label: "Artworks"
#       f.has_many :event_batches, heading: "Dates", allow_destroy: true,
#         new_record: "Add Date" do |batch|
#         batch.input :start_date, as: :date_picker, input_html: { style:
#           "width:75% !important" }
#         batch.input :end_date, as: :date_picker, input_html: { style:
#           "width:75% !important" }
#       end
#     end
#     f.actions
#   end
#   permit_params :id, :title, :description, :location, :long, :lat,
#     artwork_ids: [], event_batches_attributes: [:id, :event_id, :start_date,
#     :end_date, :_destroy]
# end