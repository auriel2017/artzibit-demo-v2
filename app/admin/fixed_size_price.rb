ActiveAdmin.register FixedSizePrice do
  menu if: proc{ current_admin_user.admin? }
  menu parent: "Fixed Sizes", label: "Price"
  form do |f|
    @fixed_sizes = controller.instance_variable_get(:@fixed_sizes)
    @fixed_sizes = FixedSize.all if @fixed_sizes.blank?
    f.inputs do
      f.input :fixed_size, input_html: { class: "chosen-input",
        style: "width:75% !important", readonly: true },
        collection: @fixed_sizes.map{|u|[u.name, u.id]}, include_blank: false
      f.input :price
      f.input :effectivity_date
    end
    f.actions
  end
  permit_params :id, :fixed_size_id, :price, :effectivity_date
  controller do
    def new
      @fixed_sizes = FixedSize.where id: params[:fixed_size_id]
      super
    end
    def create
      super do |success,failure|
        success.html { redirect_to admin_fixed_size_path(resource.fixed_size) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_fixed_size_path(resource.fixed_size) }
      end
    end
  end
end