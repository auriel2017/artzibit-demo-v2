ActiveAdmin.register Cart do
  menu if: proc{ current_admin_user.admin? }
  form do |f|
    @chose = "chosen-input"
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :user, collection: User.all.map{|u|[u.name, u.id]}, prompt: true,
        include_blank: true, input_html: {style: "width:75% !important", onchange: "populate_user_shipping_detail(this.id);", class: "chosen-input"}
      f.input :user_shipping_detail_id, collection: option_groups_from_collection_for_select(User.all,
          :user_shipping_details, :name, :id, :name_w_addr, f.object.user_shipping_detail_id),
          prompt: true, include_blank: true, input_html: {onclick: "populate_user_shipping_detail('cart_user_id');"}
      f.input :status, as: :select, collection: [nil, "checkout"], prompt: false, include_blank: false
      f.input("fixed_size_prices", label: "", input_html: {style: "display:none"},
        collection: option_groups_from_collection_for_select(FixedSize.all,
        :fixed_size_prices, :name, :id, :price))
      f.input("fixed_size_frame_prices", label: "", input_html: {style: "display:none"},
        collection: option_groups_from_collection_for_select(FixedSize.all,
        :fixed_size_frame_prices, :name, :id, :price))
      f.input :delivery_price, input_html: {style: "width:75% !important", class: "chosen-input"},
          collection: DeliveryPrice.all.map{|u|[u.name, u.id]}, prompt: false,
          include_blank: false
      f.input :created_by_admin, label: "", input_html: {value: true, hidden: true}
      f.has_many :cart_items, heading: "Artworks", allow_destroy: true,
        new_record: "Add Artwork", input_html: {class: "chosen-input"} do |art|
        art.input :artwork, collection: Artwork.all.map{|u|[u.name, u.id]},
          allow_blank: true, prompt: "Select an Artwork", input_html: {class: "artwork_select chosen-input",
          style: "width:75% !important", onclick: "trigger_chose(this)", onchange: "populate_artwork_size(this.
          id);"}
        art.input :fixed_size, allow_blank: true, include_blank: true,
          input_html: {style: "width:75% !important", onchange: "populate_fixed_size(this.id);populate_fixed_frame_size(this.id);",
          class: "chosen-input", onclick: "trigger_chose(this)"}, collection: FixedSize.all.map{|u|[u.name, u.id]}, prompt: "Select a Size"
        art.input :fixed_size_price, input_html: {style: "width:75% !important"},
          collection: option_groups_from_collection_for_select(FixedSize.all,
          :fixed_size_prices, :name, :id, :price, art.object.fixed_size_price_id),
          allow_blank: true
        art.input :height, as: :number, label: "Height (leave blank if not scalable)"
        art.input :width, as: :number, label: "Width (leave blank if not scalable)"
        art.input :quantity, as: :number
        art.input :installation, as: :boolean, value: false
        art.input :installation_price, input_html: {style: "width:75% !important"},
          collection: InstallationPrice.all.map{|u|[u.name, u.id]}, prompt: false,
          include_blank: false
        # art.input :base_price,  input_html: {style: "width:75% !important"},
        #   collection: BasePrice.all.map{|u|[u.name, u.id]}, prompt: false,
        #   include_blank: false
        art.input :frame, as: :boolean, value: false
        art.input :fixed_size_frame_price, input_html: {style: "width:75% !important"},
          collection: option_groups_from_collection_for_select(FixedSize.all,
          :fixed_size_frame_prices, :name, :id, :price, art.object.fixed_size_frame_price_id),
          allow_blank: true
        art.input :mark_up_price, input_html: { class: "chosen-input", style: "width:75% !important" },
          collection: MarkUpPrice.all.map{|u|["#{u.percentage}% (#{u.effectivity_date})", u.id]}
      end
    end
    f.actions
  end
  permit_params :user_id, :user_shipping_detail_id, :status, :delivery_price_id,
    :created_by_admin, cart_items_attributes: [:id, :cart_id, :_destroy, :artwork_id,
    :fixed_size_id, :fixed_size_price_id, :frame, :fixed_size_frame_price_id,
    :height, :width, :quantity, :installation, :installation_price_id,
    :mark_up_price_id]
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :user
        row :status
        row "Sub Total Amount" do
          resource.sub_total_amount("true")
        end
        row "Delivery Charge" do
          resource.delivery_charge_amount("true")
        end
        row "Promo Code" do
          h3 resource.promo_code.try(:code)
        end
        row "Discount Amount" do
          h3 resource.discount_amount
        end
        row "Total Amount" do
          h3 resource.total_price
        end
        row "Order" do
          (link_to resource.order.try(:name), admin_order_path(resource.order)) unless resource.order.blank?
        end
        row "Shipping Details" do
          (link_to resource.order.user_shipping_detail.try(:name_w_addr), admin_user_shipping_detail_path(resource.order.user_shipping_detail)) unless resource.order.try(:user_shipping_detail).blank?
        end
      end
    end
    panel "Items" do
      table_for resource.cart_items.each do
        column "ID" do |x|
          x.id
        end
        column "Artwork" do |x|
          x.artwork
        end
        column "Size" do |x|
          x.size
        end
        column "Qty" do |x|
          x.quantity
        end
        column "Base Price" do |x|
          "#{x.total_base_price('true')} #{'(' + x.base_price('true').to_s + ')' unless x.quantity == 1}"
        end
        column "MarkUp Price" do |x|
          "#{x.markup_percentage('true')}%"
        end
        column "Product Price" do |x|
          "#{x.total_product_amount('true')} #{'(' + x.product_amount('true').to_s + ')' unless x.quantity == 1}"
        end
        column "Installation Price" do |x|
          "#{x.total_installation_amount('saved')} #{'(' + x.installation_amount('saved').to_s + ')' unless x.quantity == 1}"
        end
        column "Frame Price" do |x|
          "#{x.total_frame_amount('saved')} #{'(' + x.frame_amount('saved').to_s + ')' unless x.quantity == 1}"
        end
        # column "Base Price" do |x|
        #   "#{x.current_base_price} (#{x.base_price.try(:name)})"
        # end
        column "Total Amount" do |x|
          x.total_amount('true')
        end
      end
    end
  end
  index do
    column :id
    column "User" do |cart|
      link_to User.find_by(id: cart.user_id).try(:name), admin_user_path(id: cart.user_id)
    end
    column :status
    column :total_price
    column "Items count" do |x|
      x.items_count
    end
    column :created_at
    actions
  end
end