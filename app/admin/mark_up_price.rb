ActiveAdmin.register MarkUpPrice do
  form do |f|
    f.inputs do
      f.input :artwork, input_html: { class: "chosen-input", style: "width:75% !important"}, collection: Artwork.all.map{|u|[u.name, u.id]}, include_blank: false
      f.input :percentage
      f.input :effectivity_date
    end
    f.actions
  end
  permit_params :id, :artwork_id, :percentage, :effectivity_date
end