ActiveAdmin.register ContentRepository do
  menu if: proc{ current_admin_user.admin? }
  menu false
#   actions :all
#   filter :title
#   filter :subtitle
#   filter :sequence_no
#   filter :redirect_to
#   index do
#     selectable_column
#     column :id
#     column :title
#     column :subtitle
#     column :description
#     column :sequence_no
#     column :redirect_to
#     actions
#   end
#   permit_params :description, :html_content, :redirect_to, :sequence_no, :subtitle, :title, :carousel_image
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :sequence_no, as: :number
#       f.input :title
#       f.input :subtitle
#       f.input :description, as: :text
#       f.input :carousel_image, as: :file
#       f.input :redirect_to
#       f.input :html_content, as: :string
#     end
#     f.actions
#   end
#   show do
#     panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
#       attributes_table_for resource do
#         row :sequence_no
#         row :title
#         row :subtitle
#         row :description
#         row 'Carousel Image' do
#           image_tag url_for(resource.carousel_image.url)
#         end
#       end
#     end
#   end
end