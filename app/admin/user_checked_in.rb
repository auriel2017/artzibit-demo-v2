# ActiveAdmin.register UserCheckedIn do
#   actions :all, except: :show
#   index do
#     selectable_column
#     column :event_batch
#     column :user
#     column :checked_in_at
#     actions
#   end
#   form do |f|
#     f.semantic_errors *f.object.errors.keys
#     f.inputs do
#       f.input :event_batch, input_html:   { class: "chosen-input", style: "width:75% !important" }, collection: EventBatch.all.map{|u|["#{u.event.title} - (#{u.format_date}) ", u.id]}, include_blank: false
#       f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.all.map{|u|[u.name, u.id]}, include_blank: false
#       f.input :checked_in_at
#     end
#     f.actions
#   end
#   permit_params :id, :event_batch_id, :user_id, :checked_in_at
# end