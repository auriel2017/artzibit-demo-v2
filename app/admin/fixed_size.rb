ActiveAdmin.register FixedSize do
  menu if: proc{ current_admin_user.admin? }
  actions :all
  index do
    selectable_column
    column :id
    column "Type" do |x|
      FixedSize::SIZE_TYPES[x.size_type]
    end
    column "Label" do |x|
      FixedSize::SIZE_LABELS[x.label]
    end
    column :height
    column :width
    column "Current Price" do |x|
      x.current_price
    end
    column "Current Frame Price" do |x|
      x.current_frame_price
    end
    column "Actions" do |row|
      (link_to "Add Price", new_admin_fixed_size_price_path(fixed_size_id: row),
        method: :get) + " " +
      (link_to "Add Frame Price", new_admin_fixed_size_frame_price_path(fixed_size_id: row),
        method: :get)
    end
    actions
  end
  action_item only: :show do
    (link_to "Add Price", new_admin_fixed_size_price_path(fixed_size_id: resource),
        method: :get) + " " +
    (link_to "Add Frame Price", new_admin_fixed_size_frame_price_path(fixed_size_id: resource),
        method: :get)
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :id
        row "Type" do
          FixedSize::SIZE_TYPES[resource.size_type]
        end
        row "Label" do
          FixedSize::SIZE_LABELS[resource.label]
        end
        row :height
        row :width
      end
    end
    panel "Price History" do
      table_for resource.fixed_size_prices.order(:effectivity_date) do
        column :price
        column :effectivity_date
      end
    end
    panel "Frame Price History" do
      table_for resource.fixed_size_frame_prices.order(:effectivity_date) do
        column :price
        column :effectivity_date
      end
    end
  end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs do
      f.input :size_type, as: :radio, collection: FixedSize::SIZE_TYPES.map{|key,value| [value,key]}
      f.input :label, as: :select, collection: FixedSize::SIZE_LABELS.map{|key,value| [value,key]}
      f.input :height
      f.input :width
    end
    f.actions
  end
  permit_params :id, :label, :size_type, :height, :width
end