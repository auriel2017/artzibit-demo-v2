ActiveAdmin.register Artwork do
  menu if: proc{ current_admin_user.admin? }
  scope :actives, default: true
  scope :deleted
  actions :all
  filter :name
  filter :user_name, as: :string, label: "Artist"
  # filter :size_option_name, as: :string, label: "Size Option"
  # filter :category_name, as: :string, label: "Category"
  filter :artwork_style_name, as: :string, label: "Style"
  filter :size_option_name, as: :string, label: "Size Option"
  filter :limited_edition, as: :boolean, label: "Limited Edition"
  index do
    selectable_column
    column :id
    column :name
    column "Artist" do |art|
      # art.user(:name)
      User.find_by(id: art.user_id).try(:name)
    end
    # column "Size Option" do |art|
    #   art.size_option(:name)
    # end
    # column "Category" do |art|
    #   art.category(:name)
    # end
    column "Status" do |art|
      art.status
    end
    column "Privacy" do |art|
      art.privacy
    end
    column "Size Option" do |art|
      SizeOption.find_by(id: art.size_option_id).try(:name)
    end
    column "Style" do |art|
      ArtworkStyle.find_by(id: art.artwork_style_id).try(:name)
    end
    column "Limited Edition" do |art|
      art.limited_edition.nil? ? false : art.limited_edition
    end
    column "Current Mark Up" do |art|
      "#{art.markup_price(art.featured_price).to_s.to_d.to_f.round(2)} (#{art.current_markup.try(:percentage).to_i}%)"
    end
    column "Price" do |art|
      "#{art.featured_final_price} (#{art.featured_price})"
    end
    column "Description" do |art|
      truncate art.description
    end
    column "Sales" do |art|
      art.total_sales.to_s.to_d.to_f.round(2)
    end
    column "Pieces Sold" do |art|
      art.total_pieces_sold
    end
    column "Favorites" do |art|
      art.total_favorites
    end
    # column "Actions" do |row|
    #   (link_to "Add #{'Min ' if row.scalable?}Size", new_admin_artwork_size_path(artwork_id: row), method: :get)  + " " +
    #   (link_to "Add #{'Base ' if row.scalable?}Price", new_admin_artwork_price_path(artwork_id: row), method: :get) + " " +
    #   ((link_to "Add Price per Unit", new_admin_price_per_unit_path(artwork_id: row), method: :get) if row.scalable?)
    # end
    actions
  end
  # action_item only: :show do
  #   (link_to "Add #{'Min ' if resource.scalable?}Size", new_admin_artwork_size_path(artwork_id: resource), method: :get)  + " " +
  #   (link_to "Add #{'Base ' if resource.scalable?}Price", new_admin_artwork_price_path(artwork_id: resource), method: :get) + " " +
  #   ((link_to "Add Price per Unit", new_admin_price_per_unit_path(artwork_id: resource), method: :get) if resource.scalable?)
  # end
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs multipart: true do
      f.input :modified_admin_user_id, label: " ", input_html: {value: current_admin_user.id, hidden: true}
      f.input :active
      f.input :name
      f.input :description
      f.input :size_option, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: SizeOption.all.map{|u|[u.name, u.id]}, include_blank: true
      f.input :artwork_style, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: ArtworkStyle.all.map{|u|[u.name, u.id]}
      f.input :user, input_html: { class: "chosen-input", style: "width:75% !important" }, collection: User.all.map{|u|[u.name, u.id]}
      f.input :privacy, input_html: { class: "chosen-input", style: "width:75% !important" },
        collection: [["Public", "public"], ["Private", "private"]], include_blank: true
      f.input :limited_edition, input_html: {onclick: "display_quantity_field(this);", disabled: (action_name == "edit")}
      f.input :quantity, input_html: {disabled: (action_name == "edit")}
      f.has_many :artwork_images, heading: "Images", allow_destroy: true, new_record: "Add Image" do |img|
        if img.object.new_record?
          img.input :image, as: :file
        else
          img.input :image, as: :file, hint: (img.object.image.blank? ?
            "No Image" : image_tag(img.object.image.url(:medium)) )
        end
      end
      f.actions
    end
  end
  show do
    panel(I18n.t('active_admin.details', model: active_admin_config.resource_label)) do
      attributes_table_for resource do
        row :active
        row :name
        row :description
        row "User" do
          resource.user(:name)
        end
        row "Privacy" do
          resource.privacy
        end
        row "Size Option" do
          resource.size_option(:name)
        end
        row "Style" do
          resource.artwork_style(:name)
        end
        row "Limited Edition" do
          resource.limited_edition.nil? ? false : resource.limited_edition
        end
        if (resource.limited_edition.nil? ? false : resource.limited_edition)
          row "Quantity" do
            resource.quantity
          end
        end
        row :size_type
        row "Current Price" do
          resource.featured_price
        end
        row "Images" do
          resource.artwork_images.map{|a| image_tag url_for(a.image.url(:medium))}.
            join("&nbsp;&nbsp;").html_safe unless resource.artwork_images.blank?
        end
        row "Dimensions" do
          resource.artwork_images.first.image.aspect_ratio
        end
      end
    end
    panel "Price Mark Up" do
      table_for resource.mark_up_prices.order(:effectivity_date) do
        column :percentage
        column :effectivity_date
      end
    end
    panel "History" do
      table_for resource.artwork_histories.order(:id) do
        column :field_name
        column :old_value
        column :new_value
        column "Modified By" do |x|
          x.admin_user.try(:name)
        end
      end
    end
    # panel "#{'Min' if resource.scalable?} Size(s)" do
    #   resource.artwork_sizes.order(:height).each do |x|
    #     unless x.artwork_prices.blank?
    #       b (link_to("#{'Featured:' if x.featured} #{x.height} X #{x.width} #{'(Available Until: ' + x.available_until.strftime('%b %d %Y') + ')' unless x.available_until.blank?}", admin_artwork_size_path(x)))
    #       table_for x.artwork_prices.order("effectivity_date desc") do
    #         column "Price" do |s|
    #           "#{s.price}"
    #         end
    #         column "Effectivity Date" do |s|
    #           s.effectivity_date
    #         end
    #       end
    #     else
    #       b (link_to("#{'Featured:' if x.featured} #{x.height} X #{x.width} #{'(Available Until: ' + x.available_until.strftime('%b %d %Y') + ')' unless x.available_until.blank?}", admin_artwork_size_path(x)))
    #       table_for x do
    #         column "Price" do |s|
    #           "Not Entered Price"
    #         end
    #       end
    #     end
    #   end
    # end
    # if resource.scalable?
    #   panel "Price per unit" do
    #     table_for resource.price_per_units.order(:effectivity_date) do
    #       column :number_per_unit
    #       column :price
    #       column :effectivity_date
    #     end
    #   end
    # end
  end
  permit_params :id, :description, :name, :artwork_style_id, :size_option_id,
    :limited_edition, :quantity, :user_id, :privacy, :modified_admin_user_id,
    :active, artwork_images_attributes: [:id, :artwork_id, :_destroy, :image]
  controller do
    def destroy
      if resource.can_be_deleted?
        begin
          resource.destroy
        rescue
          resource.active = false
          resource.save
        end
      else
        resource.active = false
        resource.save
      end
      redirect_to admin_user_artworks_path
    end
  end
end