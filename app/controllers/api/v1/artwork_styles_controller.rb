class Api::V1::ArtworkStylesController < UserModuleController
  def index
    @artwork_styles = ArtworkStyle.all.map{|as| as.v1_format}
    render json: {status: true, data: @artwork_styles}
  end
end