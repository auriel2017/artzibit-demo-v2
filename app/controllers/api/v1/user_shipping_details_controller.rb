class Api::V1::UserShippingDetailsController < UserModuleController
  def index
    @shipping_details = current_user.user_shipping_details.actives.map{|a| a.v1_format}
    render json: {status: true, data: @shipping_details}
  end
  def create
    @shipping_detail = UserShippingDetail.new(user_ship_dtl_params)
    @shipping_detail.user_id = current_user.id
    if @shipping_detail.save
      render json: {status: true, data: @shipping_detail}
    else
      render json: {status: false, message: @shipping_detail.errors.
        full_messages.first}
    end
  end
  def update
    @shipping_detail = UserShippingDetail.find_by id: params[:id]
    unless @shipping_detail.blank?
      if @shipping_detail.update_attributes(user_ship_dtl_params)
        render json: {status: true, message: "Address succesfully updated"}
      else
        render json: {status: false, message: @shipping_detail.errors.
          full_messages.first}
      end
    else
      render json: {status: false, message: "Cannot find Address"}
    end
  end
  def destroy
    @shipping_detail = UserShippingDetail.find_by id: params[:id]
    unless @shipping_detail.blank?
      if @shipping_detail.can_be_deleted?
        begin
          @shipping_detail.destroy
        rescue
          @shipping_detail.active = false
          @shipping_detail.save(validate: false)
        end
      else
        @shipping_detail.active = false
        @shipping_detail.save(validate: false)
      end
      render json: {status: true, message: "Address succesfully deleted"}
    else
      render json: {status: false, message: "Cannot find Address"}
    end
  end
  private
  def user_ship_dtl_params
    params.required(:user_shipping_detail).permit(:id, :first_name, :last_name,
      :phone_number, :detailed_addr, :city, :country, :is_default, :email)
  end
end