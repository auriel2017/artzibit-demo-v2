class Api::V1::SizeOptionsController < UserModuleController
  respond_to :json
  def index
    @size_options = SizeOption.all.map{|c| c.v1_format }
    respond_with status: true, data: @size_options
  end
end