class Api::V1::UserLaunchAppCountersController < UserModuleController
  def create
    @user_launch = UserLaunchAppCounter.where(user_view_params).first_or_initialize
    @user_launch.counter = @user_launch.counter.to_i + 1
    if @user_launch.save
      render json: {status: true, data: @user_launch}
    else
      render json: {status: false, message: @user_launch.errors.full_messages.
        first}
    end
  end
  private
  def user_view_params
    params.required(:user_launch).permit(:user_id)
  end
end