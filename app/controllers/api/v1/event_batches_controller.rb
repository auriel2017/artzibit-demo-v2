class Api::V1::EventBatchesController < UserModuleController
  def index
    @event_batches = EventBatch.all
    render json: {status: true, data: @event_batches.map{|eb| eb.v1_format}}
  end
  def show
    @event_batch = EventBatch.find_by id:params[:id]
    unless @event_batch.blank?
      render json: {status: true, data: @event_batch.v1_format("show")}
    else
      render json: {status: false, message: "No Event Found"}
    end
  end
end