class Api::V1::CartsController < UserModuleController
  # before_filter :update_installation_when_overseas, only: [:update]
  # before_filter :default_in_shipping_dtls, only: [:update]
  # before_filter :default_in_token, only: [:update]
  def index
    @cart = Cart.where("user_id = ? and (status = ? or status = ? or status IS NULL)",
      current_user.id, "", nil)
    unless @cart.blank?
      @cart = @cart.first
      @upd_cart = update_cart_items_price
      @change = {change: (@upd_cart.total_price != @cart.total_price)}
      @upd_cart = @upd_cart.v1_format
      render json: {status: true, data: @upd_cart.merge(@change) }
    else
      render json: {status: false, message: "No found cart"}
    end
  end
  def show
    @cart = Cart.find_by(id: params[:id])
    unless @cart.blank?
      render json: {status: true, data: @cart.v1_format}
    else
      render json: {status: false, message: "No found cart"}
    end
  end
  def create
    @cart = Cart.new cart_params
    @cart.user_id = current_user.id
    if @cart.save
      render json: {status: true, data: @cart}
    else
      render json: {status: false, message: @cart.errors.full_messages.first}
    end
  end
  def update
    @cart = Cart.where("id = ? and (status = ? or status = ? or status IS NULL)",
      params[:id], "", nil).first

    unless @cart.blank?
      # IF CHECKOUT
      if cart_params[:status] == "checkout"
        @error = ""

        #check if updated
        @upd_cart = update_cart_items_price
        @change = (@upd_cart.total_price != @cart.total_price)
        if @change
          @error = "Changes in Price"
          @location = "review"
          @change = {change: (@change)}
          @upd_cart = @upd_cart.v1_format
        end

        # check if shipping parameters are present
        if cart_params[:user_shipping_detail_id].blank?
          @error = "No Shipping Detail Found"
          @location = "shipping"
        end

        # check if shipping parameters are present
        if token_params.blank?
          @error = "No Card Detail Found"
          @location = "payment"
        end

        # check if card details parameters are present
        if @error.blank?
          customer_stripe_id = current_user.stripe_id
          if token_params[:type] == "card"
            @customer = Stripe::Customer.retrieve(current_user.stripe_id)
            begin
              @card_detail = @customer.sources.retrieve(token_params[:number])
            rescue
              @error = "Cannot find card"
              @location = "payment"
            else
              @cart.card_stripe_id = @card_detail.id
            end
          else
            @error = "Wrong parameters for Card Details"
            @location = "payment"
          end
          if current_user.stripe_id.blank?
            @error = "No created customer"
            @location = "payment"
          end
        end

        if @error.blank?
          if @cart.update_attributes(cart_params)
            order = Order.new
            order.user_id = @cart.user_id
            order.cart_id = @cart.id
            order.user_shipping_detail_id = @cart.user_shipping_detail_id
            order.card_stripe_id = @cart.card_stripe_id
            order.current_status = "order"
            if order.save
              order_hist = OrderHistory.new
              order_hist.order_id = order.id
              order_hist.status = "order"
              order_hist.processed_at = @cart.updated_at
              if order_hist.save
                  charge = Stripe::Charge.create(
                    :customer    => current_user.stripe_id,
                    :amount      => (@cart.total_price * 100).to_i,
                    :description => @cart.charge_description,
                    :source      => @cart.card_stripe_id,
                    :currency    => 'usd'
                  )
                  render json: {status: true, data: order}
              else
                render json: {status: false, message: "Contact Admin for Error 1: #{order_hist.errors.full_messages.first}"}
              end
            else
              render json: {status: false, message: "Contact Admin for Error 2:  #{order.errors.full_messages.first}"}
            end
          else
            render json: {status: false, message: @cart.errors.full_messages.first,
              data: @cart.v1_format}
          end
        else
          if @change
            render json: {status: false, data: @upd_cart.merge(@change) }
          else
            render json: {status: false, message: @error, location: @location}
          end
        end
      # IF NOT CHECKOUT
      else
        if !shipping_dtl_params.blank?
          @user_shipping_dtl = UserShippingDetail.new shipping_dtl_params
          @user_shipping_dtl.user_id = current_user.id
          if @user_shipping_dtl.save
            @shipping_details = current_user.user_shipping_details.actives.map{|a| a.v1_format}
            render json: {status: true, data: @shipping_details}
          else
            render json: {status: false, message: @user_shipping_dtl.errors.full_messages.first}
          end
        elsif !token_params.blank?
          customer_stripe_id = current_user.stripe_id
          if token_params[:type] == "stripe"
            if customer_stripe_id.blank? # No Added Card Yet
              current_user.add_stripe_id token_params[:number]
              if current_user.stripe_id.blank?
                render json: {status: false, message: "Please enter your card details again"}
              else
                @customer = Stripe::Customer.retrieve(current_user.stripe_id)
                if @customer.blank?
                  render json: {status: false, message: "Please enter your card details again"}
                else
                  @bank_details = current_user.user_bank_details.map{|a| a.v1_format}[0]
                  @bank_details = [] if @bank_details.blank?
                  render json: {status: true, data: @bank_details}
                end
              end
            else # There is existing Card(s)
              @customer = Stripe::Customer.retrieve(customer_stripe_id)
              begin
                @card_detail = @customer.sources.create({:source => token_params[:number]})
              rescue
                render json: {status: false, message: "Please enter your card details again"}
              else
                if token_params[:is_default] == true
                  @customer.default_source = @card_detail.id
                  @customer.save
                end
                @bank_details = current_user.user_bank_details.map{|a| a.v1_format}[0]
                @bank_details = [] if @bank_details.blank?
                render json: {status: true, data: @bank_details}
              end
            end
          else
            render json: {status: false, message: "Wrong parameters for Card Details"}
          end
        elsif cart_params[:actual_promo_code] == ""
          render json: {status: false, message: "Promo Code is invalid"}
        else
          if @cart.update_attributes(cart_params)
            render json: {status: true, data: @cart.v1_format}
          else
            render json: {status: false, message: @cart.errors.full_messages.first,
              data: @cart.v1_format}
          end
        end
      end
    else
      render json: {status: false, message: "No Found Cart"}
    end
  end

  def update_cart_items_price
    @cart.cart_items.each do |item|
      if item.total_amount != item.total_amount("save")
        item = CartItem.update_to_current_price_fields item.id
        item.save(validate: false)
      end
    end
    cart = Cart.update_cart_amount @cart.id
    cart.save
    cart
  end
  private
  # def update_installation_when_overseas
  #   shipping_dtl_country = "Singapore"
  #   if params[:user_shipping_details].blank?
  #     shipping_dtl = UserShippingDetail.find params[:cart][:user_shipping_detail_id]
  #     unless shipping_dtl.blank?
  #       shipping_dtl_country = shipping_dtl.country
  #     end
  #   else
  #     shipping_dtl = params[:user_shipping_details].select{|token| token[:is_selected] == true}
  #   end
  # end
  # def default_in_shipping_dtls
  #   unless params[:user_shipping_details].blank?
  #     unless params[:cart][:user_shipping_detail_id].blank?
  #       if params[:user_shipping_details].select{|token| token[:is_selected] == true}.count >= 1
  #         render json: {status: false, message: "You select an existing Shipping Detail"}
  #       end
  #     else
  #       unless params[:user_shipping_details].select{|token| token[:is_selected] == true}.count == 1
  #         render json: {status: false, message: "You select 0 or more than 1 Shipping Details"}
  #       end
  #     end
  #   end
  # end
  # def default_in_token
  #   unless params[:tokens].blank?
  #     unless params[:tokens].select{|token| token[:is_selected] == true}.count == 1
  #       render json: {status: false, message: "You select either 0 or more than 1 Bank Details"}
  #     end
  #   end
  # end
  def token_params
    # params.permit(tokens: [:type, :number, :is_selected])
    params.required(:token).permit(:type, :number, :is_default) unless params[:token].blank?
  end
  def shipping_dtl_params
    # params.permit(user_shipping_details: [:id, :first_name, :last_name,
    #   :phone_number, :detailed_addr, :city, :country, :is_default, :is_selected]) unless params[:user_shipping_details].blank?
    params.required(:user_shipping_detail).permit(:id, :first_name, :last_name,
      :phone_number, :detailed_addr, :city, :country, :is_default, :is_selected,
      :email) unless params[:user_shipping_detail].blank?
  end
  def cart_params
    if action_name != "create" and !params[:cart][:cart_items_attributes].blank?
      cart_items_params = params[:cart][:cart_items_attributes]
      cart_items_params.count.to_i.times do |x|
        cart_item = cart_items_params[x]
        cart_item_id = cart_item[:id]
        cart_item_qty = cart_item[:quantity]
        unless cart_item_id.blank?
          params[:cart][:cart_items_attributes][x][:quantity] = cart_item_qty
        else
          if params[:cart][:cart_items_attributes][x][:frame] == false
            params[:cart][:cart_items_attributes][x][:fixed_size_frame_price_id] = nil
          end
          param_wo_qty = cart_item.except(:quantity)
          param_wo_qty = param_wo_qty.except(:mark_up_price_id) if param_wo_qty[:mark_up_price_id].blank?
          param_wo_qty = param_wo_qty.except(:fixed_size_price_id)
          param_wo_qty = param_wo_qty.except(:fixed_size_frame_price_id)

          cart_id = CartItem.find_by(param_wo_qty)
          unless cart_id.blank?
            addtl_param = {"id" => cart_id.id, "quantity" => (cart_id.quantity + cart_item_qty.to_i)}
            new_cart_item = cart_item.merge(addtl_param)
            params[:cart][:cart_items_attributes][x] = cart_item.merge(new_cart_item)
          end
        end
      end
    end
    if params[:cart].blank?
      params[:cart][:status] = nil
    end
    params.required(:cart).permit(:id, :user_id, :status, :stripe_token,
      :user_shipping_detail_id, :card_stripe_id, :actual_promo_code,
      cart_items_attributes: [:id, :cart_id, :artwork_id, :height, :width,
      :quantity, :_destroy, :frame, :fixed_size_id, :fixed_size_price_id,
      :installation, :installation_price_id, :fixed_size_frame_price_id,
      :mark_up_price_id])
  end
end