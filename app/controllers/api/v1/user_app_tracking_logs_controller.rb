class Api::V1::UserAppTrackingLogsController < UserModuleController
  def create
    unless user_app_tracking_log_param[:data].blank?
      user_app_tracking_log_param[:data].each do |user_app_tracking|
        @user_log = UserAppTrackingLog.new user_app_tracking
        @user_log.save
        unless @user_log.errors.blank?
          @error = @user_log.errors.full_messages.first
          break
        end
      end
    end
    if @error.blank?
      render json: {status: true, data: @error}
    else
      render json: {status: false, message: @error}
    end
  end
  private
  def user_app_tracking_log_param
    params.required(:user_app_tracking_logs).permit(data: [:artwork_id, :user_id,
      :start_time, :end_time])
  end
end