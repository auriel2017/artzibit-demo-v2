class Api::V1::ArtworksController < ApplicationController
  acts_as_token_authentication_handler_for User
  before_action :authenticate_user!
  respond_to :json
  def index
    @page_per_artwork = params[:page]
    if @page_per_artwork.blank?
      offset = 15 * 1.to_i
      limit = (15 - offset).abs
    else
      offset = 15 * params[:page].to_i
      limit = (15 - offset).abs
    end
    if params[:privacy].blank?
      @artworks = Artwork.all.actives.
      filter_by_artwork_style(params[:artwork_style_id]).
      filter_by_artist(params[:artist_id]).
      filter_by_privacy(params[:privacy]).
      reject{|a| (a.limited_edition == true and a.current_quantity <= 0)}.
      sort_by{|a| [(a.total_sales.to_i * -1), (a.total_favorites.to_i * -1), (a.created_at.to_i * -1)]}
    else
      @artworks = Artwork.all.actives.
      filter_by_artist(params[:artist_id]).
      filter_by_privacy(params[:privacy]).
      order(:id)
    end
    unless params[:all] == "true"
      @artworks = @artworks.uniq.first(offset).drop(limit)
    end

    render json: {status: true, data: @artworks.map{|a| a.v1_format("index",
      (current_user.blank? ? nil : current_user.id))} }
  end
  def show
    @artwork = Artwork.find_by_id params[:id]
    unless @artwork.blank?
      render json: {status: true, data: @artwork.v1_format("show",
        (current_user.blank? ? nil : current_user.id))}
    else
      render json: {status: false, message: "No Artwork found."}
    end
  end
  def new
    @styles = ArtworkStyle.all.map{|as| as.v1_format}
    render json: {status: true, data: @styles }
  end
  def edit
    @artwork = Artwork.find_by id: params[:id]
    unless @artwork.blank?
      if @artwork.user_id == current_user.id
        render json: {status: true, data: @artwork.v1_format}
      else
        render json: {status: false, message: "You have no right to update this artwork"}
      end
    else
      render json: {status: false, message: "No Artwork found."}
    end
  end
  def create
    @artwork = Artwork.new artwork_params
    @artwork.user_id = current_user.id
    if @artwork.save
      render json: {status: true, data: @artwork.v1_format("index",
      (current_user.blank? ? nil : current_user.id))}
    else
      render json: {status: false, message: @artwork.errors.full_messages.first}
    end
  end
  def update
    @artwork = Artwork.find_by id: params[:id]
    unless @artwork.blank?
      unless check_mark_up_params.blank?
        if @artwork.update_attributes(check_mark_up_params)
          render json: {status: true, data: @artwork.v1_format("show"), message: "Artwork updated successfully"}
        else
          render json: {status: false, message: @artwork.errors.full_messages.first}
        end
      else
        if @artwork.update_attributes(artwork_params)
          render json: {status: true, data: @artwork.v1_format("show"), message: "Artwork updated successfully"}
        else
          render json: {status: false, message: @artwork.errors.full_messages.first}
        end
      end
    else
      render json: {status: false, message: "No Artwork Found"}
    end
  end
  def destroy
    @artwork = Artwork.find_by id: params[:id]
    unless @artwork.blank?
      @artwork.destroy
      render json: {status: true, message: "Artwork successfully deleted"}
    else
      render json: {status: false, message: "No Artwork found."}
    end
  end
  def destroy
    @artwork = Artwork.find_by id: params[:id]
    unless @artwork.blank?
      if @artwork.can_be_deleted?
        begin
          @artwork.destroy
        rescue
          @artwork.active = false
          @artwork.save
        end
      else
        @artwork.active = false
        @artwork.save
      end
      render json: {status: true, message: "Artwork succesfully deleted"}
    else
      render json: {status: false, message: "Cannot find Artwork"}
    end
  end
  private
  def check_mark_up_params
    artwork = Artwork.find_by id: params[:id]
    new_params = artwork_params
    unless artwork.blank?
      mark_up_params =  artwork_params[:mark_up_prices_attributes]
      unless mark_up_params.blank?
        if mark_up_params.first[:percentage] == artwork.current_markup.try(:percentage)
          new_params = artwork_params.except(:mark_up_prices_attributes)
        end
      end
    end
    new_params
  end
  def artwork_params
    if action_name == "create"
      params_avatar = params[:artwork][:artwork_images_attributes].first[:image] = params[:artwork][:artwork_images_attributes].first[:image]
      params[:artwork][:artwork_images_attributes].first[:image] = params[:artwork][:artwork_images_attributes].first[:image] = format_image(params_avatar)
      params.required(:artwork).permit(:id, :name, :description, :artwork_style_id,
        :size_option_id, :limited_edition, :quantity, :privacy,
        artwork_images_attributes: [:id, :image], mark_up_prices_attributes: [:id,:percentage])
    else
      params.required(:artwork).permit(:id, :name, :description, :artwork_style_id,
        :size_option_id, :privacy, artwork_images_attributes: [:id, :image],
        mark_up_prices_attributes: [:id,:percentage])
    end
  end
  def format_image!
    params_avatar = artwork_params[:artwork_images_attributes].first[:image]
    unless params_avatar.blank?
      avatar = params_avatar
      artwork_params[:artwork_images_attributes].first[:image] = format_image avatar
    end
  end
  def format_image image_data
    data = nil
    unless image_data.blank?
      data = StringIO.new(Base64.decode64(image_data))
      data.class.class_eval {attr_accessor :original_filename, :content_type}
      data.original_filename = "User/Image" + Time.now.to_s.gsub(" ","_")
      data.content_type = "image/jpg"
    end
    return data
  end
  # def authenticate_user!
  #   unless current_user
  #     #do nothing
  #   end
  # end
end