class Api::V1::ArtworkMarkersController < UserModuleController
  def create
    # begin
      ArtworkMarkerMailer.sending_of_marker(current_user).deliver_now
    # rescue
      # render json: {status: false, message: "Sending email failed"}
    # else
      render json: {status: true, message: "The AR Marker has been sent to your email."}
    # end
  end
end