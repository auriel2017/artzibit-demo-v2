class Users::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    @bg_image = "microsite-bg-half-pink.jpg"
    @color = "#777";
    super
  end

  # POST /resource/sign_in
  # def create
  #   super do |success,failure|
  #     success.html { redirect_to root_path }
  #     failure.html { redirect_to root_path }
  #   end
  # end

  def create
    super
    # build_resource(sign_up_params)

    # if resource.save
    #   yield resource if block_given?
    #   if resource.active_for_authentication?
    #       set_flash_message :notice, :signed_up if is_flashing_format?
    #       sign_up(resource_name, resource)
    #       respond_with resource, location: after_sign_up_path_for(resource)
    #   else
    #       set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
    #       expire_data_after_sign_in!
    #       respond_with resource, location: after_inactive_sign_up_path_for(resource)
    #   end
    # else
    #     clean_up_passwords resource
    #     resource.errors.full_messages.each {|x| flash[:error] = x} # Rails 4 simple way
    #     redirect_to root_path
    # end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  protected
  # def auth_options
  #   { :scope => resource_name, :recall => "Page#home" }
  # end
  # def build_resource(hash=nil)
  #   self.resource = resource_class.new_with_session(hash || {}, session)
  # end
  # def sign_up_params
  #   devise_parameter_sanitizer.sanitize(:sign_up)
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end

end
