//= require active_admin/base
//= require chosen-jquery
//= require jquery-ui
//= require scaffold
//= require subas/main

$(function(){
   $(".chosen-input").chosen();
});

// $(document).ready(function() {
  // var add_btn = document.getElementsByClassName("has_many_add")[0];
  function trigger_chose(e) {
    // alert("asdadas");
    $(".chosen-input").chosen();
    // $('#autoship_option').val('').trigger('chosen:updated');
    // alert(document.getElementsByClassName("artwork_select").length);
    // e.trigger("chosen:updated");
  }
// });

$(document).ready(function() {
  artwork_sizes = $('#cart_artwork_sizes').html();
  artwork_prices = $('#cart_artwork_prices').html();
  price_per_units = $('#cart_price_per_units').html();
  $('#cart_artwork_sizes_input').hide();
  $('#cart_artwork_prices_input').hide();
  $('#cart_price_per_units_input').hide();
  $('#cart_fixed_size_prices_input').hide();
  $('#cart_fixed_size_frame_prices_input').hide();

  $('.has_many_add').click(function(){
   setTimeout(function() {
    var cusid_ele = document.getElementsByClassName('dynamic-select');
    for (var i = 0; i < cusid_ele.length; ++i) {
      // $(cusid_ele[i]).empty()
    }
   }, 100);
  });

});

function display_quantity_field(e) {
  quantity = document.getElementById("artwork_quantity_input");
  if (e.checked == true) {
    quantity.style.display = "block";
  }
  else {
    quantity.style.display = "none";
  }
}

function populate_artwork_size(e) {
  $(".chosen-input").chosen();
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
  populate_artwork_price("cart_cart_items_attributes_" + idx + "_artwork_size_id")
  populate_price_per_unit("cart_cart_items_attributes_" + idx + "_price_per_unit_id")
}

function populate_artwork_price(e) {
  $(".chosen-input").chosen();
  l = e.indexOf("_artwork_size_id");
  idx = e.substring(27,l);
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  artwork_price = '#cart_cart_items_attributes_' + idx + '_artwork_price_id';
  user = $(artwork_size + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_prices).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_price).html(options)
  $(artwork_price).show()
}

function populate_price_per_unit(e) {
  $(".chosen-input").chosen();
  l = e.indexOf("_price_per_unit_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
  price_per_unit = '#cart_cart_items_attributes_' + idx + '_price_per_unit_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(price_per_units).filter("optgroup[label='" + escaped_user + "']").html()
  $(price_per_unit).html(options)
  $(price_per_unit).show()
}

function populate_user_shipping_detail(e) {
  l = e.indexOf("_artwork_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_artwork_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_artwork_size_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
  populate_artwork_price("cart_cart_items_attributes_" + idx + "_artwork_size_id")
  populate_price_per_unit("cart_cart_items_attributes_" + idx + "_price_per_unit_id")
}

function populate_fixed_size(e) {
  artwork_sizes = $('#cart_fixed_size_prices').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_fixed_size_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_fixed_size_price_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
}

function populate_fixed_frame_size(e) {
  artwork_sizes = $('#cart_fixed_size_frame_prices').html();
  l = e.indexOf("_fixed_size_id");
  idx = e.substring(27,l);
  artwork = '#cart_cart_items_attributes_' + idx + '_fixed_size_id';
  artwork_size = '#cart_cart_items_attributes_' + idx + '_fixed_size_frame_price_id';
  user = $(artwork + ' :selected').text()
  escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
  options = $(artwork_sizes).filter("optgroup[label='" + escaped_user + "']").html()
  $(artwork_size).html(options)
  $(artwork_size).show()
}