#= require active_admin/base

jQuery ->
  $('#artwork_price_artwork_size_id').parent().hide()
  students = $('#artwork_price_artwork_size_id').html()
  $('#artwork_price_artwork_id').change ->
    user = $('#artwork_price_artwork_id :selected').text()
    escaped_user = user.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(students).filter("optgroup[label='#{escaped_user}']").html()
    if options
      $('#artwork_price_artwork_size_id').html(options)
      $('#artwork_price_artwork_size_id').parent().show()
    else
      $('#artwork_price_artwork_size_id').empty()
      $('#artwork_price_artwork_size_id').empty()
