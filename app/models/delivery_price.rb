class DeliveryPrice < ActiveRecord::Base
  belongs_to :delivery_range
  validates :price, :effectivity_date, :delivery_range_id, presence: true
  def self.current_price
    price = DeliveryPrice.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last
    price
  end
  def name
    "$#{price}: Order with Artwork size between (#{delivery_range.min_size}cm - #{delivery_range.max_size}cm) #{' - with Installation' if delivery_range.with_installation?}"
  end
end