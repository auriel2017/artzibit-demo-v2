class UserShippingDetail < ActiveRecord::Base
  belongs_to :user
  has_many :orders

  scope "actives", -> {where("active = ?", true)}
  scope "deleted", -> {where("active = ?", false)}

  validates :user_id, :first_name, :last_name, :phone_number,
    :detailed_addr, :city, :country, :email, presence: true
  validates :detailed_addr, uniqueness: {scope: [:first_name, :last_name, :phone_number,
    :city, :country, :user_id, :email]}
  after_save :remove_all_default
  after_destroy :add_new_default

  attr_accessor :is_selected

  def add_new_default
    shippings = UserShippingDetail.where("user_id = ? and is_default = ?",
      user_id, true)
    if shippings.blank?
      last_shipping = user.user_shipping_details.last
      last_shipping.is_default = true
      last_shipping.save(validate: false)
    end
  end
  def can_be_deleted?
    orders.count == 0
  end
  def remove_all_default
    if is_default == true
      users = UserShippingDetail.where("id != #{id} and user_id = #{user_id}
        and is_default is #{true}")
      users.update_all(is_default: false)
    end
  end
  def name
    "#{first_name} #{last_name}"
  end
  def name_w_addr
    "#{name} - #{detailed_addr}, #{country}"
  end
  def v1_format
    {
      id: id,
      email: email,
      name: name,
      first_name: first_name,
      last_name: last_name,
      phone_number: phone_number,
      address: detailed_addr,
      country: country,
      city: city,
      is_default: is_default
    }
  end
end
