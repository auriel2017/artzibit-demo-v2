class Ability
  include CanCan::Ability

  def initialize(admin_user)
    can :dashboard
    if admin_user.admin?
      can :manage, :all
    end
  end

end