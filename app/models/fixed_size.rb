class FixedSize < ActiveRecord::Base
  SIZE_LABELS = {"small" => "Small", "medium" => "Medium", "large" => "Large",
    "xlarge" => "Extra Large"}
  SIZE_TYPES = {"square" => "Square", "landscape" => "Landscape",
    "portrait" => "Portrait"}
  validates :label, :size_type, :height, :width, presence: true
  validates :label, uniqueness: {scope: [:size_type, :height, :width]}
  has_many :fixed_size_prices
  has_many :fixed_size_frame_prices
  has_many :cart_items
  def name
    "#{SIZE_TYPES[size_type]}: #{SIZE_LABELS[label]}"
  end
  def name_with_no
    # if size_type == "portrait"
    #   "#{SIZE_LABELS[label]}: #{height}cm X #{width}cm"
    # else
      "#{SIZE_LABELS[label]}: #{width}cm X #{height}cm"
    # end
  end

  def current_fixed_size_price
    # fixed_size_prices.where("effectivity_date <= ?", Time.now).order("effectivity_date desc").first
    FixedSizePrice.where("fixed_size_id = ? and effectivity_date <= ?", id, Time.now).order("effectivity_date desc").first
  end
  def current_fixed_size_frame_price_id
    current_fixed_size_frame_price.try(:id)
  end
  def current_fixed_size_price_id
    current_fixed_size_price.try(:id)
  end
  def current_price
    current_fixed_size_price.try(:price)
  end

  def current_fixed_size_frame_price
    fixed_size_frame_prices.where("effectivity_date <= ?", Time.now).order("effectivity_date desc").first
  end
  def current_frame_price
    current_fixed_size_frame_price.try(:price)
  end

  def v1_format artwork=nil
    current_p = current_price.to_s.to_d.to_f
    current_p += artwork.markup_price(current_price)

    height_min = CartItem.minimum_size("height", size_type, height)
    width_min = CartItem.minimum_size("width", size_type, width)

    height_amt_per_cm = CartItem.amt_per_cm("height",size_type,height_min.try(:height),height_min.try(:current_price)).to_s.to_d.to_f
    height_amt_per_cm += artwork.markup_price(height_amt_per_cm)

    width_amt_per_cm = CartItem.amt_per_cm("width",size_type,width_min.try(:width),width_min.try(:current_price)).to_s.to_d.to_f
    width_amt_per_cm += artwork.markup_price(width_amt_per_cm)

    {
      label: name_with_no,
      fixed_size_id: id,
      height: height.to_i,
      width: width.to_i,
      fixed_size_price_id: current_fixed_size_price.try(:id),
      fixed_size_frame_price_id: current_fixed_size_frame_price.try(:id),
      price: current_p,
      height_amount_per_cm: height_amt_per_cm,
      width_amount_per_cm: width_amt_per_cm
    }
  end
end
