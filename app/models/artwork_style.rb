class ArtworkStyle < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  has_many :artworks

  has_many :promo_code_dtls, as: :promo_codable
  has_many :promo_codes, through: :promo_code_dtls

  def v1_format
    {
      id: id,
      name: name
    }
  end
end
