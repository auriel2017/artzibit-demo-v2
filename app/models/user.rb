class User < ActiveRecord::Base
  ARTIST_TYPE = {"" => "Not an artist", "photographer" => "Photographer",
    "visual_artist" => "Visual Artist"}
  acts_as_token_authenticatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable
  has_one :user_authentication, dependent: :destroy
  has_many :user_favorites, dependent: :destroy
  has_one :user_verified, dependent: :destroy
  has_many :carts, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :user_bank_details, dependent: :destroy
  has_many :user_shipping_details, dependent: :destroy
  has_many :artworks, dependent: :destroy
  has_many :user_views, dependent: :destroy
  has_many :user_launch_app_counters, dependent: :destroy
  has_many :user_app_tracking_logs, dependent: :destroy
  has_many :user_activation_codes, dependent: :destroy
  has_many :activation_codes, through: :user_activation_codes

  has_many :promo_code_dtls, as: :promo_codable, dependent: :destroy
  has_many :promo_codes, through: :promo_code_dtls

  accepts_nested_attributes_for :user_authentication, allow_destroy: true
  has_attached_file :avatar
  validates_attachment_content_type :avatar,
    content_type: /^image\/(jpg|jpeg|pjpeg|png|x-png|gif)$/
  validates :first_name, :last_name, :country, presence: true
  validates :username, uniqueness: true
  validate :number_per_activation_code
  attr_accessor :reserved_token, :current_password
  scope :artists, -> {where("artist_type = 'photographer' or artist_type = 'visual_artist' ")}
  scope :photographers, -> {where("artist_type = 'photographer' ")}
  scope :visual_artists, -> {where("artist_type = 'visual_artist' ")}
  scope :artists, -> {where("artist_type = 'photographer' or artist_type =
    'visual_artist'")}
  after_create :update_activation_code, unless: "by_pass_activation_code == true"

 def self.authenticate(params={})
      user = User.find_for_authentication(:email => params[:email])
      return nil if user.blank?
      user.valid_password?(params[:password]) ? user : nil
  end
  def artwork_sequence
    first_artwork = artworks.actives.order("id asc").first.try(:id)
  end

  def index_in_first_artwork
    ind_order = User.all.map{|a| [a.id, a.artwork_sequence]}.sort{|a,b| a[1].to_i <=> b[1].to_i}.reject{|a| a[1].nil?}.map.with_index{|a,index| [a[0],index]}.select{|a| a[0] == id}[0]
    (ind_order[1] + 1) unless ind_order.blank?
  end

  def update_activation_code
    @activation_code = UserActivationCode.find_by reserved_token: reserved_token
    @activation_code.user_id = id
    @activation_code.save
  end

  def number_per_activation_code
    if by_pass_activation_code == false
      if id.blank?
        @activation_code = UserActivationCode.find_by reserved_token: reserved_token
        unless @activation_code.blank?
          if @activation_code.expiration < Time.now
            errors.add(:activation_code_id, "Reserve Token expires")
          end
        else
          # @user = User.find_by id: self.id
          # if @user.blank?
            errors.add(:activation_code_id, "Activation Code Not found")
          # end
        end
      end
    end
  end
  #################
  def artwork_view_counter
    user_views.map{|u| u.counter}.inject(0){|sum,x| sum + x}
  end
  def app_launch_counter
    user_launch_app_counters.map{|u| u.counter}.inject(0){|sum,x| sum + x}
  end
  def artwork_view_duration
    total_duration = user_app_tracking_logs.artwork.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end
  def app_stay_duration
    total_duration = user_app_tracking_logs.app.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end
  #################
  def add_stripe_id token
    customer = Stripe::Customer.create(
      :email => email,
      :source => token
    )
    UserBankDetail.create(user_id: id, stripe_id: customer.id)
  end
  def stripe_id
    user_bank_details.order("updated_at desc").first.try(:stripe_id)
  end
  def stripe_details
    stripe_id
    # unless stripe_id.blank?
    #   customer = Stripe::Customer.retrieve(stripe_id)
    #   customer.sources.map{|s| "CARD NO: *****#{s.last4} EXP: #{s.exp_month}/#{s.exp_year}"}.join(" <br>").html_safe
    # end
  end
  def password_required?
    super && user_authentication.blank?
  end
  def confirmation_required?
    if log_thru_facebook?
      skip_confirmation!
      false
    end
  end
  def log_thru_facebook?
    user_authentication.present?
  end
  def auth_exists?
    if User.find_by_email(email).present?
      unless auth.blank?
        true
      else
        false
      end
    else
      false
    end
  end
  def auth
    user = User.find_by_email(email)
    uauth = user_authentication
    unless UserAuthentication.where(provider: uauth.provider, uid: uauth.uid,
      user_id: user.id).blank?
      user
    else
      nil
    end
  end
  def verified
    if user_verified.blank?
      false
    else
      !user_verified.deleted
    end
  end
  def verify
    user_verify = UserVerified.new(user_id: self.id, deleted:false, effectivity_date: Time.now)
    user_verify
  end
  def unverify
    user_unverify = UserVerified.find_by_user_id self.id
    user_unverify.deleted = !user_unverify.deleted
    user_unverify
  end
  def last_shipping_detail
    ship_details = user_shipping_details.actives.where(is_default: true)
    ship_details = user_shipping_details.actives.order(:created_at).first if ship_details.blank?
    ship_details
  end
  def v1_format
    {
      id: id,
      name: name,
      artworks: artworks.actives.count
    }
  end
end