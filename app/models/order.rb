class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :cart
  has_many :cart_items, through: :cart, dependent: :destroy
  has_many :order_histories, dependent: :destroy
  has_many :order_couriers, dependent: :destroy
  has_many :order_assignments, dependent: :destroy
  belongs_to :user_shipping_detail

  scope :order_submitted, -> {where(current_status: "order") }
  scope :printing, -> { where(current_status: "printing") }

  scope :ready_to_deliver, -> { where(current_status: "ready_to_deliver") }
  scope :on_delivery, -> { where(current_status: "on_delivery") }
  scope :completed, -> { where(current_status: "completed") }

  scope :assigned_ready_to_deliver, -> { where(current_status: "ready_to_deliver") }
  scope :assigned_on_delivery, -> { where(current_status: "on_delivery") }
  scope :assigned_completed, -> { where(current_status: "completed") }

  after_create :send_receipt
  after_save :update_artwork_fields

  def update_artwork_fields
    order = Order.find_by id: self.id
    order.cart_items.each do |ci|
      artwork = Artwork.find_by id: ci.artwork_id
      unless artwork.blank?
        artwork.total_sales = artwork.sales
        artwork.total_pieces_sold = artwork.pieces_sold
        artwork.save(validate: false)
      end
    end
  end

  def send_receipt
    OrderMailer.send_receipt(self).deliver_now
    # OrderMailer.notify_printer(self, AdminUser.default_printer_email.email).deliver_now unless AdminUser.default_printer_email.blank?
    OrderMailer.notify_printer(self, "gravides.dren@gmail.com").deliver_now
    OrderMailer.notify_assignment(self, "gravides.dren@gmail.com").deliver_now
  end

  def name
    "Order ##{id}"
  end
  def file_name
    "Order#{id}_#{user.username}"
  end
  def courier
    order_couriers.order("created_at desc").first
  end

  def assignee
    order_assignments.order("created_at desc").first.try(:admin_user)
  end

  def installation?
    cart.cart_items.select{|a| a.installation}.count > 0
  end

  def v1_format mode=""
    if mode == ""
    {
      id: id,
      status: OrderHistory::STATUS[current_status],
      no_of_items_in_order: cart.items_count,
      updated_date: updated_at.strftime("%Y-%m-%d"),
      updated_time: updated_at.strftime("%I:%M%p")
    }
    else
    {
      courier: courier_name,
      tracking_no: id,
      logistics_tracking: order_histories.order("created_at").map{|oh| oh.v1_format}
    }
    end
  end

  def courier_name
    if (current_status == "on_delivery" or current_status == "completed")
      if order_assignments.blank?
        AdminUser.printer_name
      else
        assignee.try(:name)
      end
    end
  end

  def card_stripe
    begin
      customer = Stripe::Customer.retrieve(user.stripe_id)
    rescue Stripe::InvalidRequestError
      nil
    else
      unless card_stripe_id.blank?
        begin
          card_detail = customer.sources.retrieve(card_stripe_id)
        rescue Stripe::InvalidRequestError
          nil
        else
          card_detail
        end
      end
    end
  end
  def card_description
    card_detail = card_stripe
    unless card_detail.blank?
    "BRAND: #{card_detail.brand} CARD NO: *****#{card_detail.last4} EXP: #{card_detail.exp_month}/#{card_detail.exp_year}"
    end
  end
  def last_process
    order_histories.order("processed_at desc").first
  end
end
