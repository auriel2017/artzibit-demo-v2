class ArtworkHistory < ActiveRecord::Base
  belongs_to :artwork
  belongs_to :admin_user

  after_create :create_artwork_approval, if: "field_name == 'privacy' and
    new_value == 'public'"

  def create_artwork_approval
    if artwork.right_aspect_ratio? == true
      ArtworkApprovalHdr.create(artwork_id: artwork.id, expiration_date: Time.now + 48.hours)
    end
  end
end
