class Artwork < ActiveRecord::Base
  SIZE_TYPE = ['Fixed', 'Scalable', 'Presets']
  SIZE_FILTER = {"small"=> {min_h: 0, min_w: 0, max_h: 10, max_w: 10},
    "medium"=> {min_h: 10.01, min_w: 10.01, max_h:15, max_w:15},
    "large"=> {min_h: 15.01, min_w: 15.01, max_h: 20, max_w: 20},
    "xlarge"=> {min_h: 20.01, min_w: 20.01, max_h: 50, max_w: 50}}
  PRICE_FILTER = {"small"=> {min_p: 0, max_p: 30},
    "medium"=> {min_p: 30.01, max_p: 50},
    "large"=> {min_p: 50.01, max_p: 100},
    "xlarge"=> {min_p: 100.01, max_p: 9999}}

  attr_accessor :modified_admin_user_id

  scope :actives, -> {where(active: true)}
  scope :deleted, -> {where(active: false)}
  scope :public_artworks, -> {where(privacy: "public")}
  scope :private_artworks, -> {where(privacy: "private")}

  # belongs_to :category
  belongs_to :artwork_style
  belongs_to :artist
  belongs_to :user
  belongs_to :size_option
  # has_many :event_artworks
  # has_many :events, through: :event_artworks
  # has_many :artwork_sizes , dependent: :destroy
  has_many :artwork_images, dependent: :destroy
  has_many :artwork_prices, dependent: :destroy
  has_many :price_per_units, dependent: :destroy
  has_many :user_favorites, dependent: :destroy
  has_many :user_views, dependent: :destroy
  has_many :user_app_tracking_logs, dependent: :destroy
  has_many :cart_items, dependent: :destroy
  has_many :mark_up_prices, dependent: :destroy
  has_many :artwork_approval_hdrs, dependent: :destroy
  has_many :artwork_histories, dependent: :destroy

  has_many :promo_code_dtls, as: :promo_codable, dependent: :destroy
  has_many :promo_codes, through: :promo_code_dtls

  accepts_nested_attributes_for :artwork_images, allow_destroy: true
  accepts_nested_attributes_for :mark_up_prices, allow_destroy: true


  validates :name, :artwork_style_id, :user_id, :privacy,
    presence: true
  validates :quantity, presence: true, if: "limited_edition == true"
  validates :name, uniqueness: {scope: [:artwork_style_id, :user_id]}
  # validates :artwork_images, presence: true
  validate :user_limit, if: "id.blank?"

  before_save :check_quantity_if_limited_edition
  after_create :update_user_upload_index
  after_create :check_if_user_index_not_unique
  after_create :create_artwork_approval, if: "privacy == 'public'"
  after_create :send_submission_email, if: "privacy == 'public'"
  before_update :send_submission_email_after_crop, if: "privacy == 'public'"
  before_update :create_history


  def send_submission_email_after_crop
    if self.artwork_images.first.cropping?
      ArtworkMailer.send_submission(self).deliver_now
    end
  end

  def send_submission_email
    if right_aspect_ratio? == true
      ArtworkMailer.send_submission(self).deliver_now
    end
  end

  def can_be_deleted?
    cart_items.count == 0
  end

  def create_artwork_approval
    ArtworkApprovalHdr.create(artwork_id: self.id, expiration_date: Time.now + 48.hours)
  end

  def create_history
    fields = ["name", "category_id", "artwork_style_id", "description", "privacy"]
    old_artwork = Artwork.find_by id: id
    fields.each do |att|
      if old_artwork.send(att) != self.send(att)
        case att
          when "category_id"
            old_value = Category.find_by id: old_artwork.category_id
            new_value = Category.find_by id: self.category_id
          when "artwork_style_id"
            old_value = ArtworkStyle.find id: old_artwork.artwork_style_id
            new_value = ArtworkStyle.find id: self.artwork_style_id
          else
            old_value  = old_artwork.send(att)
            new_value = self.send(att)
        end
        ArtworkHistory.create(
          artwork_id: self.id,
          field_name: att,
          admin_user_id: modified_admin_user_id,
          old_value: old_value,
          new_value: new_value,
        )
      end
    end
  end

  def last_markup
    MarkUpPrice.where(artwork_id: id).order("id desc").first
  end

  def check_quantity_if_limited_edition
    unless self.limited_edition
      self.quantity = nil
    end
  end

  def user_limit
    unless user.blank?
      seq_no = user.artwork_sequence_number.to_i
      upload_limit = UploadArtworkLimit.where("from_counter <= ? and ? <= to_counter", seq_no, seq_no)
      unless upload_limit.blank?
        errors.add(:base, "Users form 1 - 100 can only upload #{upload_limit.first.limit} number of artworks.") unless user.artworks.actives.count < upload_limit.first.limit
      end
    end
  end

  def update_user_upload_index
    @user = User.find_by id: user_id
    unless @user.blank?
      if @user.artwork_sequence_number.blank?
        @user.artwork_sequence_number = @user.index_in_first_artwork
        @user.save(validate: false)
      end
    end
  end

  def check_if_user_index_not_unique
    @user = User.find_by id: user_id
    unless @user.blank?
      unless @user.artwork_sequence_number.blank?
        @same_index = User.where("id != ? and artwork_sequence_number = ?",
          @user.id, @user.artwork_sequence_number)
        unless @same_index.blank?
          @user_with_index = User.where("artwork_sequence_number IS NOT NULL")
          @user.artwork_sequence_number = @user_with_index.count + 1
        end
        @user.save(validate: false)
      end
    end
  end

  def sales
    Order.all.includes(:cart_items).map{|o| o.cart_items.select{|ci| ci.artwork_id == self.id}.
      map{|ci| ci.total_product_amount('true')}.inject(0){|sum,x| sum + x }}.inject(0){|sum,x| sum + x }
  end
  def pieces_sold
    Order.all.includes(:cart_items).map{|o| o.cart_items.select{|ci| ci.artwork_id == self.id}.
      map{|ci| ci.quantity}.inject(0){|sum,x| sum + x}}.inject(0){|sum,x| sum + x}
  end
  def favorites
    UserFavorite.where(artwork_id: id, deleted: false).count
  end

  def self.filter_by_privacy filter
    if filter.blank?
      where("privacy = 'public'").select{|a| a.status == "approve"}
    else
      where("privacy = ?", filter)
    end
  end
  def self.filter_by_artist filter
    if filter.blank?
      where("user_id IS NOT NULL")
    else
      where("user_id = ?", filter)
    end
  end
  def self.filter_by_category filter
    if filter.blank?
      where("category_id IS NOT NULL")
    else
      where("category_id = ?", filter)
    end
  end
  def self.filter_by_artwork_style filter
    if filter.blank?
      where("artwork_style_id IS NOT NULL")
    else
      where("artwork_style_id = ?", filter)
    end
  end
  def self.filter_by_size filter
    if filter.blank?
      joins(:artwork_sizes)
    else
      query_string = ""
      filter.split(",").each do |x|
        min_h = SIZE_FILTER[x][:min_h]
        min_w = SIZE_FILTER[x][:min_w]
        max_h = SIZE_FILTER[x][:max_h]
        max_w = SIZE_FILTER[x][:max_w]
        query_string += " or " unless query_string.blank?
        query_string += ("((artwork_sizes.height <= #{max_h} and artwork_sizes.
        width <= #{max_w} and artwork_sizes.height >= #{min_h} and artwork_sizes.
        width >= #{min_w} and (artworks.size_type = 'Fixed' or artworks.size_type
        = 'Presets')) or artworks.size_type = 'Scalable')")
      end
      joins(:artwork_sizes).where(query_string)
    end
  end
  def self.filter_by_price filter
    if filter.blank?
      joins(:artwork_prices)
    else
      query_string = ""
      filter.split(",").each do |x|
        min_p = PRICE_FILTER[x][:min_p]
        max_p = PRICE_FILTER[x][:max_p]
        query_string += " or " unless query_string.blank?
        query_string += "(#{min_p} <= artwork_prices.price and artwork_prices.price <= #{max_p})"
      end
      joins(:artwork_prices).where(query_string)
    end
  end
  def validate_featured_size
    errors.add(:size_type, "unfeature the connected size before updating the Size Type") if artwork_sizes.where(featured: true).count > 0 and size_type != "Presets"
  end

  def validate_number_of_sizes
    if ["Fixed","Scalable"].include? size_type
      errors.add(:size_type, "There are #{artwork_sizes.size} connected sizes. You cannot update.") if
      artwork_sizes.size > 1
    end
  end

  def right_aspect_ratio?
    unless featured_artwork_image.blank?
      case featured_artwork_image.orientation
        when "square"
          1 == featured_artwork_image.image.aspect_ratio.round(1)
        when "landscape"
          1.5 == featured_artwork_image.image.aspect_ratio.round(1)
        when "portrait"
          0.67 == featured_artwork_image.image.aspect_ratio.round(2)
        else
          false
      end
    end
  end

  def status
    approval = ArtworkApprovalHdr.where(artwork_id: self.id).order(:id)
    unless approval.blank?
      unless approval.last.artwork_approval_dtls.blank?
        approval.last.status
      else
        "approve"
      end
    else
      "approve"
    end
  end

  def right_aspect_ratio
    unless featured_artwork_image.blank?
      featured_artwork_image.image.aspect_ratio.round(2)
    end
  end

  def featured_image
    artwork_images.first.try(:image)
  end
  def presets?
    size_type == "Presets"
  end
  def scalable?
    size_type == "Scalable"
  end
  def faves_count
    user_favorites.where(deleted: false).count
  end
  def views_count
    user_views.map{|u| u.counter}.inject(0){|sum,x| sum + x }
  end
  def user_views_duration_count
    total_duration = user_app_tracking_logs.map{|u| u.duration}.inject(0){|sum,x| sum + x}
    Time.at(total_duration).utc.strftime("%H:%M")
  end
  def favorite user_id
    user_favorites.find_by(user_id: user_id)
  end
  def favorited? user_id
    fave = favorite(user_id)
    if fave.blank?
      false
    else
      !fave.deleted
    end
  end
  def current_quantity
    if limited_edition
      (quantity - pieces_sold)
    else
      nil
    end
  end
  def v1_format(mode = "", current_user_id = "")
    case mode
      when "show"
        {
          id: id,
          image: featured_image,
          featured_price: featured_price.to_s.to_d.to_f,
          name: name,
          artist_name: User.find_by(id: user_id).try(:name),
          # orientation: featured_artwork_image.try(:orientation),
          description: description,
          available_sizes: fixed_artwork_sizes.order(:width).map{|as| as.v1_format(self)},
          favorite_id: favorite(current_user_id).try(:id),
          favorited: favorited?(current_user_id),
          cart_id: featured_cart(current_user_id),
          privacy: privacy,
          limited_edition: (limited_edition.blank? ? false : limited_edition),
          quantity: current_quantity,
          original_quantity: quantity,
          artist_id: user.id,
          mark_up_price_id: current_markup.try(:id),
          mark_up_price_percentage: current_markup.try(:percentage),
          artwork_style_id: artwork_style_id,
          status: status
        }
      else
        {
          id: id,
          name: name,
          artist_name: User.find_by(id: user_id).try(:name),
          image: featured_image,
          # orientation: featured_artwork_image.try(:orientation),
          category_id: category_id,
          artwork_style_id: artwork_style_id,
          favorite_id: favorite(current_user_id).try(:id),
          favorited: favorited?(current_user_id),
          status: status,
          user_id: user_id,
          privacy: privacy,
          limited_edition: (limited_edition.blank? ? false : limited_edition),
          quantity: current_quantity,
          artist_id: user_id,
          mark_up_price_id: current_markup.try(:id),
          mark_up_price_percentage: current_markup.try(:percentage)
        }
    end
  end
  def featured_image
    image = featured_artwork_image.try(:image)
    unless image.blank?
      "https:" + image.url(:medium).to_s unless image.blank?
    end
  end
  def fixed_artwork_sizes
    FixedSize.where(size_type: featured_artwork_image.try(:orientation))
  end
  def featured_final_price
    featured_price + markup_price(featured_price)
  end
  def featured_price
    fixed_artwork_sizes.order(:width).first.try(:current_price)
  end
  def featured_artwork_image
    artwork_images.first
  end

  def current_markup
    mark_up_prices.where("effectivity_date <= ?", Time.now).order(:effectivity_date).last
  end

  def markup_price base_price = 0, saved=""
    unless current_markup.blank?
      mark_up_percentage = (current_markup.try(:percentage).to_s.to_d.to_f / 100).to_d.to_f
      base_price.to_s.to_d.to_f * mark_up_percentage
    else
      0
    end
  end

  # def price
  #   size.try(:latest_price)
  # end
  # def featured_size
  #   size.try(:format_size)
  # end
  # def size
  #   if size_type == "Presets"
  #     feature = artwork_sizes.where(featured: true).order(:updated_at)
  #     unless feature.blank?
  #       feature.last
  #     else
  #       artwork_sizes.order(:updated_at).last
  #     end
  #   else
  #     artwork_sizes.order(:updated_at).last
  #   end
  # end
  # def price_per_unit
  #   min_price = price_per_units.where("effectivity_date <= ?", Date.today).
  #     order("effectivity_date desc, id desc")
  #   unless min_price.blank?
  #     min_price = min_price.first
  #   else
  #     nil
  #   end
  # end
  def featured_cart user_id
    user = User.where(id: user_id)
    unless user.blank?
      user.first.carts.where(status: nil).last.try(:id)
    end
  end
  # def featured_cart_item_id cart_id
  #   cart = Cart.where(id: cart_id)
  #   unless cart.blank?
  #     cart.first.cart_items.where(artwork_id: id, artwork_size_id: size.try(:id),
  #       artwork_price_id: price.try(:id), price_per_unit_id: price_per_unit.try(:id)).
  #       last.try(:id)
  #   end
  # end
end