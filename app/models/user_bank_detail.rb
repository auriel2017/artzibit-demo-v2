class UserBankDetail < ActiveRecord::Base
  belongs_to :user
  def v1_format
    customer = Stripe::Customer.retrieve(stripe_id)
    def_source = customer.default_source
    customer.sources.map{|a| {
      id: a.id,
      is_default: (def_source == a.id),
      brand: a.brand,
      last4: a.last4,
      exp_month: a.exp_month,
      exp_year: a.exp_year
    } }
  end
end
