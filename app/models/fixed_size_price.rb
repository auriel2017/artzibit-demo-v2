class FixedSizePrice < ActiveRecord::Base
  belongs_to :fixed_size
  has_many :cart_items
  validates :price, :fixed_size, :effectivity_date, presence: true
end
