class PromoCodeDtl < ActiveRecord::Base

  AVAILABLE_TABLES = [ ["Style", "ArtworkStyle"], ["Artwork", "Artwork"] ]
  ITEMIZABLE_TYPES = %w(Artwork ArtworkStyle)

  belongs_to :promo_code
  belongs_to :promo_codable, polymorphic: true

  belongs_to :artwork_style, foreign_key: :promo_codable_id
  belongs_to :artwork, foreign_key: :promo_codable_id
  belongs_to :user, foreign_key: :promo_codable_id

  attr_accessor :promo_codable_identifier

  validates :promo_codable_id, :promo_codable_type, presence: true

  def promo_codable_identifier
    "#{promo_codable.class.to_s}-#{promo_codable_id}"
  end

  def promo_codable_identifier=(promo_codable_data)
    if promo_codable_data.present?
      promo_codable_data = promo_codable_data.split('-')
      self.promo_codable_type = promo_codable_data[0]
      self.promo_codable_id = promo_codable_data[1]
    end
  end
end