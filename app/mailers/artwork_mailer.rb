class ArtworkMailer < ActionMailer::Base
  default from: Devise.mailer_sender
  def send_submission artwork
    @artwork = artwork
    attachments.inline['ig.png'] = {
      data: File.read(Rails.root.join('app/assets/images/ig.png')),
      mime_type: 'image/png'
    }
    # attachments.inline['image.png'] = {
    #   data: open("https:" + @artwork_approval_hdr.artwork.featured_artwork_image.image.url(:medium)).read,
    #   mime_type: 'image/png'
    # }
    mail to: artwork.user.email, subject: "Artwork Submission - Artzibit"
  end
end