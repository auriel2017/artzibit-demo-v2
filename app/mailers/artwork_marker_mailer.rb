class ArtworkMarkerMailer < ActionMailer::Base
  default from: Devise.mailer_sender
  def sending_of_marker user
    attachments['ArtzibitMarker.pdf'] = File.read("#{Rails.root}/public/ArtzibitMarker.pdf") # Attached file
    mail to: user.email, subject: "Artzibit Marker"
  end
end