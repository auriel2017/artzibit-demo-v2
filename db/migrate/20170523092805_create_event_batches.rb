class CreateEventBatches < ActiveRecord::Migration
  def change
    create_table :event_batches do |t|
      t.references :event, index: true
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps null: false
    end
    add_foreign_key :event_batches, :events
  end
end
