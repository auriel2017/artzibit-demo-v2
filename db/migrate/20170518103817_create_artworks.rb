class CreateArtworks < ActiveRecord::Migration
  def change
    create_table :artworks do |t|
      t.string :name
      t.references :category
      t.references :artwork_style
      t.text :description

      t.timestamps
    end
    add_index :artworks, :category_id
    add_index :artworks, :artwork_style_id
  end
end
