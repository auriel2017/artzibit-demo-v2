class AddFieldsUser < ActiveRecord::Migration
  def change
  	add_column :users, :first_name, :string, null: false, default: ''
  	add_column :users, :last_name, :string, null: false, default: ''
  	add_column :users, :middle_name, :string
  	add_column :users, :contact_number, :string
  end
end
