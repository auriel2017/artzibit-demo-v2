class CreateArtworkHistories < ActiveRecord::Migration
  def change
    create_table :artwork_histories do |t|
      t.references :artwork, index: true
      t.string :field_name
      t.string :old_value
      t.string :new_value
      t.references :admin_user, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :artwork_histories, :artworks
    add_foreign_key :artwork_histories, :admin_users
    add_foreign_key :artwork_histories, :users
  end
end
