class AddBasePriceIdToCartItem < ActiveRecord::Migration
  def change
    add_reference :cart_items, :base_price, index: true
    add_foreign_key :cart_items, :base_prices
  end
end
