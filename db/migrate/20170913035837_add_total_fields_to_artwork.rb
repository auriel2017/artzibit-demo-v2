class AddTotalFieldsToArtwork < ActiveRecord::Migration
  def change
    add_column :artworks, :total_pieces_sold, :integer
    add_column :artworks, :total_sales, :decimal
    add_column :artworks, :total_favorites, :integer
  end
end
