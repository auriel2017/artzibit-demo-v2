class AddEmailToUserShippingDetail < ActiveRecord::Migration
  def change
    add_column :user_shipping_details, :email, :string
  end
end
