class CreateUserShippingDetails < ActiveRecord::Migration
  def change
    create_table :user_shipping_details do |t|
      t.references :user, index: true
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.text :detailed_addr
      t.string :city
      t.string :country

      t.timestamps null: false
    end
    add_foreign_key :user_shipping_details, :users
  end
end
