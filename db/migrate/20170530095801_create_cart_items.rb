class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.references :cart, index: true
      t.references :artwork, index: true
      t.references :artwork_size, index: true
      t.references :artwork_price, index: true
      t.references :price_per_unit, index: true
      t.decimal :height
      t.decimal :width
      t.integer :quantity

      t.timestamps null: false
    end
    add_foreign_key :cart_items, :carts
    add_foreign_key :cart_items, :artworks
    add_foreign_key :cart_items, :artwork_sizes
    add_foreign_key :cart_items, :artwork_prices
    add_foreign_key :cart_items, :price_per_units
  end
end
