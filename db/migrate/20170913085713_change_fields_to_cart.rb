class ChangeFieldsToCart < ActiveRecord::Migration
  def self.up
     change_column :carts, :total_price, :decimal, :precision => 10, :scale => 2
     change_column :carts, :discount_amount, :decimal, :precision => 10, :scale => 2
  end
end
